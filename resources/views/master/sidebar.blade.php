@section('sidebar')

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGASI HALAMAN</li>
            @foreach($menu as $nav)
            <li class="treeview {{ $nav->menu_class_name === current($uriPath) ? 'active menu-open' : '' }}">
                <a href="#" onclick="redirect('{{ $nav->menu_url }}')">
                <i class="fa {{ $nav->menu_icon }}"></i> <span>{{ $nav->menu_name }}</span>
                @if(sizeof($nav->child) > 0)
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                @endif
                </a>
                @if(sizeof($nav->child) > 0)
                <ul class="treeview-menu">
                    @foreach($nav->child as $chld)
                    <li class="{{ $chld->menu_class_name === end($uriPath) ? 'active' : '' }}"><a href="#" onclick="redirect('{{ $chld->menu_url }}')"><i class="fa fa-circle-o"></i>{{ $chld->menu_name }}</a></li>
                        @if($chld->menu_class_name === end($uriPath))
                            @section('pagetitle', $chld->menu_name)
                                @show
                        @endif
                    @endforeach
                </ul>
                @endif
            </li>
                @if($nav->menu_class_name === current($uriPath))
                    @section('pagetitle', $nav->menu_name)
                        @show
                @endif
            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

@show
