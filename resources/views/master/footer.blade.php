@section('footer')

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Admin LTE Version</b> 2.4.5
    </div>
    <strong>Copyright &copy; 2019 <a href="http://flochart.id" target="_blank" title="FloCHart Digital Indonesia">Dimas Kusumah</a>.</strong> All rights
    reserved.
</footer>

@show
