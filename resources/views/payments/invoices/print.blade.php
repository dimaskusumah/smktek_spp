<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Invoice</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body onload="window.print();" style="">
        <div class="wrapper">
            <!-- Main content -->
            <section class="invoice">
                <!-- title row -->
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <h2 class="page-header" style="border-bottom: none; padding-bottom: 0px;">
                            {{ $dataSchool->school_name }}
                        </h2>
                    </div>
                    <div class="col-md-6 pull-right">
                        <div class="pull-right" style="padding: 10px; border: solid 1px;">
                            BUKTI PEMBAYARAN
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row" style="margin-bottom: 20px;">
                    <div class="col-xs-4">
                        {{ $dataSchool->school_address }}
                    </div>
                </div>
                <div class="divider"></div>
                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td style="width: 15%">Diterima Dari</td>
                                    <td style="width: 3%">:</td>
                                    <td style="width: 17%">{{ $dataInvoice[0]->student->student_name }}</td>
                                    <td style="width: 15%"></td>
                                    <td style="width: 3%"></td>
                                    <td style="width: 15%"></td>
                                    <td style="width: 15%">Tanggal Bayar</td>
                                    <td style="width: 3%">:</td>
                                    <td style="width: 17%">{{ $dataInvoice[0]->paid->paid_date }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Induk</td>
                                    <td>:</td>
                                    <td>{{ $dataInvoice[0]->student->student_register_number }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>No. Bukti</td>
                                    <td>:</td>
                                    <td>{{ $dataInvoice[0]->invoice_no }}</td>
                                </tr>
                                <tr>
                                    <td>Kelas</td>
                                    <td>:</td>
                                    <td>{{ $dataInvoice[0]->student->student_class }} {{ $dataInvoice[0]->student->majors->majors_name }}</td>
                                    <td>Status Siswa</td>
                                    <td>:</td>
                                    <td>{{ $dataInvoice[0]->student->student_status == 1 ? 'Aktif' : 'Tidak Aktif' }}</td>
                                    <td>Metode</td>
                                    <td>:</td>
                                    <td>TUNAI</td>
                                </tr>
                                <tr>
                                    <td>Terbilang</td>
                                    <td>:</td>
                                    <td colspan="4">{{ terbilang($dataInvoice[0]->paid->paid_amount) }}</td>
                                    <td>Petugas</td>
                                    <td>:</td>
                                    <td>{{ Auth::user()->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive table-striped">
                            <tbody>
                                <tr>
                                    <th style="width: 70%">Dengan rincian pembayaran sebagai berikut :</th>
                                    <td style="width: 30%"></td>
                                </tr>
                                <tr>
                                    @foreach($dataInvoice as $invoice)
                                    <td>{{ $invoice->category->category_code }} {{ $invoice->category->category_name }}</td>
                                    <td class="text-right">{{ number_format($invoice->invoice_amount, 0, '', ',') }}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-md-6" style="float: left; text-align: center;">
                                <h5>Penyetor</h5>
                                <br>
                                <br>
                                <br>
                                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                            </div>
                            <div class="col-md-6" style="float: right; text-align: center;">
                                <h5>Penerima</h5>
                                <br>
                                <br>
                                <br>
                                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Jumlah:</th>
                                        <td class="text-right">{{ number_format($dataInvoice[0]->invoice_amount, 0, '', ',') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pembayaran</th>
                                        <td class="text-right">{{ number_format($dataInvoice[0]->paid->paid_amount, 0, '', ',') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- ./wrapper -->
    </body>
</html>
