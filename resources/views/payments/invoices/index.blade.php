@extends('master.index')

@section('title', 'Payment Invoices')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Invoice</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="invoicesData" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 15%;">Nomor Invoice</th>
                                        <th style="width: 20%;">Nama Siswa</th>
                                        <th style="width: 15%;">Kelas dan Jurusan</th>
                                        <th style="width: 15%;">Nama Kategori</th>
                                        <th style="width: 10%;">Nominal</th>
                                        <th style="width: 15%;">Jatuh Tempo</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- Modal Invoice -->
                <div class="modal fade in" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Tambah</span> Data Invoice</h4>
                            </div>
                            <div class="modal-body">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" id="form-invoices" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="invoice_no" class="col-sm-4 control-label">Invoice No</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="invoice_id" value="">
                                                <input type="text" class="form-control text-right" id="invoice_no" name="invoice_no" placeholder="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id" class="col-sm-4 control-label">Kategori Pembayaran</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2" id="category" name="category_id" required>
                                                    <option value="-1" disabled selected>Pilih Kategori</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ep_id" class="col-sm-4 control-label">Nama Siswa</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2" id="student" name="student_id" required>
                                                    <option value="-1" disabled selected>Pilih Siswa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="payment_amount" class="col-sm-4 control-label">Nominal</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control money text-right" id="invoice_amount" name="invoice_amount" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice_due_date" class="col-sm-4 control-label">Jatuh Tempo</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="invoice_due_date" name="invoice_due_date" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice_notes" class="col-sm-4 control-label">Catatan</label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" id="invoice_due_date" name="invoice_notes" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                        <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Invoice">Simpan</button>
                                        <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Invoice">Simpan</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <!-- Modal Pay -->
                <div class="modal fade in" id="modal-pay" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Bayar</span> Invoice</h4>
                            </div>
                            <div class="modal-body">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" id="form-pay" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="invoice_no" class="col-sm-4 control-label">Invoice No</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="paid_invoice_id" value="">
                                                <input type="text" class="form-control" id="paid_invoice_no" name="paid_invoice_no" placeholder="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="student" class="col-sm-4 control-label">Detail Siswa</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="student" name="student" placeholder="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="payment_amount" class="col-sm-4 control-label">Nominal Dibayarkan</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control money1 text-right" id="paid_amount" name="paid_amount" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice_notes" class="col-sm-4 control-label">Catatan</label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" id="paid_notes" name="paid_notes" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                        <button type="button" class="btn btn-primary pull-right" id="pay" title="Bayar Invoice">Bayar</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_invoices.js') }}"></script>

@endsection

@section('footer')

@endsection
