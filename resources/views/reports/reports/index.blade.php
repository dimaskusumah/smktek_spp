@extends('master.index')

@section('title', 'Reporting')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Kategori Pembayaran</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="reportsData" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 30%;">Nama Siswa</th>
                                        <th style="width: 20%;">Kelas</th>
                                        <th style="width: 20%;">Kategori Pembayaran</th>
                                        <th class="sum" style="width: 15%;">Nominal Bayar</th>
                                        <th style="width: 20%;">Tanggal Bayar</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td style="font-weight: bold;">TOTAL</td>
                                        <td style="font-weight: bold;"></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_laporan.js') }}"></script>

@endsection

@section('footer')

@endsection
