@extends('master.index')

@section('title', 'Students')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Siswa</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="studentsData" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 15%;">No Induk</th>
                                        <th style="width: 15%;">NIS</th>
                                        <th style="width: 25%;">Nama</th>
                                        <th style="width: 15%;">Jenis Kelamin</th>
                                        <th style="width: 15%;">Kelas</th>
                                        <th style="width: 15%;">Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- Modal Popup -->
                <div class="modal fade in" id="modal-default" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Tambah</span> Data Siswa</h4>
                            </div>
                            <div class="modal-body">
                                <!-- Tabs -->
                                <div class="nav-tabs-custom" style="margin-bottom: 0px;">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#student" data-toggle="tab" aria-expanded="true">Data Siswa</a></li>
                                        <li class=""><a href="#student-detail" data-toggle="tab" aria-expanded="false">Detail Siswa</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="student">
                                            <!-- form start -->
                                            <form class="form-horizontal" id="form-students" >
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="student_register_number" class="col-sm-4 control-label">No Induk</label>
                                                        <div class="col-sm-6">
                                                            <input type="hidden" name="student_id" value="">
                                                            <input type="text" class="form-control" id="student_register_number" name="student_register_number" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_nisn" class="col-sm-4 control-label">NISN</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_nisn" name="student_nisn" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_name" class="col-sm-4 control-label">Nama Siswa</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_name" name="student_name" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_gender" class="col-sm-4 control-label">Jenis Kelamin</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control select2" id="genders" name="student_gender" required>
                                                                <option value="-1" disabled selected>Pilih Jenis Kelamin</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_school_year" class="col-sm-4 control-label">Tahun Masuk</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_school_year" name="student_school_year" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_class" class="col-sm-4 control-label">Kelas</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control select2" id="classes" name="student_class" required>
                                                                <option value="-1" disabled selected>Pilih Kelas</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_school_majors" class="col-sm-4 control-label">Jurusan</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control select2" id="majors" name="student_school_majors" required>
                                                                <option value="-1" disabled selected>Pilih Jurusan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_status" class="col-sm-4 control-label">Status Siswa</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control select2" id="status" name="student_status" required>
                                                                <option value="-1" disabled selected>Pilih Status</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-footer">
                                                    <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                                    <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Data Siswa">Simpan</button>
                                                    <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Data Siswa">Simpan</button>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="student-detail">
                                            <div class="alert alert-info fade in alert-dismissible" id="info-details">
                                                <strong>Info!</strong> Untuk menambahkan detail data Siswa Anda harus menambahkan dahulu data siswa kemudian klik tombol Ubah <i class="fa fa-fw fa-pencil-square-o"></i> pada tabel.
                                            </div>

                                            <form class="form-horizontal" id="form-details" >
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="student_birth_place" class="col-sm-4 control-label">Tempat Lahir</label>
                                                        <div class="col-sm-6">
                                                            <input type="hidden" name="student_detail_id" value="">
                                                            <input type="text" class="form-control" id="student_birth_place" name="student_birth_place" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_birth_day" class="col-sm-4 control-label">Tanggal Lahir</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_birth_day" name="student_birth_day" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_address" class="col-sm-4 control-label">Alamat Siswa</label>
                                                        <div class="col-sm-6">
                                                            <textarea class="form-control" id="student_address" name="student_address" placeholder="" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_phone" class="col-sm-4 control-label">No. Telepon</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_phone" name="student_phone" placeholder=""  required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_father_name" class="col-sm-4 control-label">Nama Ayah</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_father_name" name="student_father_name" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_father_jobdesc_id" class="col-sm-4 control-label">Pekerjaan Ayah</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_father_jobdesc_id" name="student_father_jobdesc_id" placeholder="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_father_phone" class="col-sm-4 control-label">No. Telepon Ayah</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_father_phone" name="student_father_phone" placeholder="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_mother_name" class="col-sm-4 control-label">Nama Ibu</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_mother_name" name="student_mother_name" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_mother_jobdesc_id" class="col-sm-4 control-label">Pekerjaan Ibu</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_mother_jobdesc_id" name="student_mother_jobdesc_id" placeholder="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="student_mother_phone" class="col-sm-4 control-label">Telepon Ibu</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="student_mother_phone" name="student_mother_phone" placeholder="" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-footer">
                                                    <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                                    <button type="button" class="btn btn-primary pull-right" id="edit-detail" title="Simpan Perubahan Detail Data Siswa">Simpan</button>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form
                                        </div>
                                    </div>
                                    <!-- /.tab-content -->
                                </div>

                                <!-- /.box-header -->
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_data_siswa.js') }}"></script>

@endsection

@section('footer')

@endsection
