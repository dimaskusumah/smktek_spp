@extends('master.index')

@section('title', 'Application Settings')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">

            @include('settings.apps.ep')

        </div>
    </div>

</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_tahun_ajaran.js') }}"></script>

@endsection

@section('footer')

@endsection
