<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Tahun Ajaran</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-12 col-xs-12">

                <table id="epData" class="table table-bordered table-striped dataTable">
                    <thead>
                        <tr>
                            <th style="width: 40%;">Tahun Pendidikan</th>
                            <th style="width: 40%;">Kurikulum Pendidikan</th>
                            <th style="width: 20%;">Action</th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- ./box-body -->

    <!-- Modal Popup -->
    <div class="modal fade in" id="modal-default" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"><span id="header-label">Tambah</span> Tahun Ajaran</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" id="form-ep">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="ep_year" class="col-sm-4 control-label">Tahun Ajaran</label>
                                <div class="col-sm-6">
                                    <input type="hidden" name="ep_id" value="">
                                    <input type="text" class="form-control" id="ep_year" name="ep_year" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ep_curriculum_year" class="col-sm-4 control-label">Kurikulim Pelajaran</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="ep_curriculum_year" name="ep_curriculum_year" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                            <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Tahun Ajaran">Simpan</button>
                            <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Tahun Ajaran">Simpan</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- /.box -->
