@extends('master.index')

@section('title', 'Majoring Class')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Jurusan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="majoringData" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">Nama Jurusan</th>
                                        <th style="width: 40%;">Keterangan Jurusan</th>
                                        <th style="width: 25%;">Kepala Jurusan</th>
                                        <th style="width: 15%;">Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- Modal Popup -->
                <div class="modal fade in" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Tambah</span> Jurusan</h4>
                            </div>
                            <div class="modal-body">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" id="form-majors">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="majors_name" class="col-sm-4 control-label">Nama Jurusan</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="majors_id" value="">
                                                <input type="text" class="form-control" id="majors_name" name="majors_name" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="majors_description" class="col-sm-4 control-label">Keterangan Jurusan</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="majors_description" name="majors_description" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="majors_head" class="col-sm-4 control-label">Kepala Jurusan</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="majors_head" name="majors_head" placeholder="" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                        <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Jurusan">Simpan</button>
                                        <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Jurusan">Simpan</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_jurusan.js') }}"></script>

@endsection

@section('footer')

@endsection
