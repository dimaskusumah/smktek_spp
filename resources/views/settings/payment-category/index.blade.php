@extends('master.index')

@section('title', 'Payment Category')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Kategori Pembayaran</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="payCategory" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">Kode Kategori</th>
                                        <th style="width: 35%;">Nama Kategori</th>
                                        <th style="width: 25%;">Deskripsi</th>
                                        <th style="width: 15%;">Status</th>
                                        <th style="width: 15%;">Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- Modal Popup -->
                <div class="modal fade in" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Tambah</span> Kategori Pembayaran</h4>
                            </div>
                            <div class="modal-body">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" id="form-payment-category" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="category_code" class="col-sm-4 control-label">Kode Kategori</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="category_id" value="">
                                                <input type="text" class="form-control" id="category_code" name="category_code" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_name" class="col-sm-4 control-label">Nama Kategori</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="category_name" name="category_name" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_description" class="col-sm-4 control-label">Deskripsi Kategori</label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="category_description" name="category_description" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_status" class="col-sm-4 control-label">Status Kategori</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2" id="status" name="category_status" required>
                                                    <option value="-1" disabled selected>Pilih Status</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                        <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Kategori Pembayaran">Simpan</button>
                                        <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Kategori Pembayaran">Simpan</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_kategori_pembayaran.js') }}"></script>

@endsection

@section('footer')

@endsection
