@extends('master.index')

@section('title', 'Payment Parameter')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Parameter Pembayaran</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <table id="payParams" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">Kategori Pembayaran</th>
                                        <th style="width: 35%;">Tahun Ajaran</th>
                                        <th style="width: 25%;">Nominal</th>
                                        <th style="width: 15%;">Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- Modal Popup -->
                <div class="modal fade in" id="modal-default" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><span id="header-label">Tambah</span> Parameter Pembayaran</h4>
                            </div>
                            <div class="modal-body">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" id="form-payment-parameter" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="payment_category_id" class="col-sm-4 control-label">Kategori Pembayaran</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2" id="category" name="payment_category_id" required>
                                                    <option value="-1" disabled selected>Pilih Kategori</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ep_id" class="col-sm-4 control-label">Tahun Ajaran</label>
                                            <div class="col-sm-6">
                                                <select class="form-control select2" id="ep" name="ep_id" required>
                                                    <option value="-1" disabled selected>Pilih Tahun Ajaran</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="payment_amount" class="col-sm-4 control-label">Nominal</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="payment_id" value="">
                                                <input type="text" class="form-control money text-right" id="payment_amount" name="payment_amount" placeholder="" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal" title="Batal">Batal</button>
                                        <button type="button" class="btn btn-primary pull-right" id="save" title="Simpan Parameter Pembayaran">Simpan</button>
                                        <button type="button" class="btn btn-primary pull-right" id="edit" title="Simpan Perubahan Parameter Pembayaran">Simpan</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_parameter_pembayaran.js') }}"></script>

@endsection

@section('footer')

@endsection
