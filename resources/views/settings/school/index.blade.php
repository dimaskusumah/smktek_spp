@extends('master.index')

@section('title', 'School Information')

@section('header')
    @parent

    <style media="screen">
        .info-box-content {
            padding: 22px 10px;
        }

        ul {
            list-style-type: none !important;
            padding: 0px !important;
        }
    </style>
@endsection

@section('content')

<section class="content">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Informasi Sekolah</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="form-school">
                    <div class="row">
                        <input type="hidden" name="school_id" value="{{ $getData->school_id }}">
                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="school_name">Nama Sekolah</label>
                                    <input type="text" class="form-control" id="school_name" name="school_name" placeholder="" value="{{ $getData->school_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="school_headmaster">Nama Kepala Sekolah</label>
                                    <input type="text" class="form-control" id="school_headmaster" name="school_headmaster" placeholder="" value="{{ $getData->school_headmaster }}">
                                </div>
                                <div class="form-group">
                                    <label for="school_address">Alamat Sekolah</label>
                                    <textarea class="form-control" id="school_address" name="school_address" placeholder="">{{ $getData->school_address }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="school_phone">Telepon</label>
                                    <input type="text" class="form-control" id="school_phone" name="school_phone" placeholder="" value="{{ $getData->school_phone }}">
                                </div>
                                <div class="form-group">
                                    <label for="school_fax">Fax</label>
                                    <input type="text" class="form-control" id="school_fax" name="school_fax" placeholder="" value="{{ $getData->school_fax }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="school_description">Tentang Sekolah</label>
                                    <textarea class="form-control" id="school_description" name="school_description" placeholder="">{{ $getData->school_description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="school_name">Logo Sekolah</label>
                                    <div class="text-center" style="margin-top: 25px;">
                                        <img src="http://localhost:8000/storage/school/logo/{{ $getData->school_logo }}" width="150px" class="text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" class="btn btn-primary pull-right" id="save">Simpan</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</section>

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('/js/spp_js/spp_informasi_sekolah.js') }}"></script>

@endsection

@section('footer')

@endsection
