<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    // Table name
    protected $table = 'spp_student';
    protected $primaryKey = 'student_id';
    protected $fillable = [
        'student_id',
        'student_nisn',
        'student_name',
        'student_gender',
        'student_school_year',
        'student_class',
        'student_school_majors',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];

    public function details()
    {
        return $this->hasOne('App\Settings\StudentsDetail', 'student_id');
    }

    public function majors()
    {
        return $this->belongsTo('App\Settings\Majors', 'student_school_majors');
    }
}
