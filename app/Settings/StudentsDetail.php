<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class StudentsDetail extends Model
{
    // Table name
    protected $table = 'spp_student_detail';
    protected $primaryKey = 'student_detail_id';
    protected $fillable = [
        'student_detail_id',
        'student_id',
        'student_birth_place',
        'student_birth_day',
        'student_address',
        'student_phone',
        'student_father_name',
        'student_father_jobdesc_id',
        'student_father_phone',
        'student_mother_name',
        'student_mother_jobdesc_id',
        'student_mother_phone',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];
}
