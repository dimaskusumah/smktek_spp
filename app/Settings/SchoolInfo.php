<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class SchoolInfo extends Model
{
    // Table name
    protected $table = 'spp_school_parameters';
    protected $primaryKey = 'school_id';
    protected $fillable = [
        'school_name',
        'school_logo',
        'school_headmaster',
        'school_address',
        'school_phone',
        'school_fax',
        'school_description',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];
}
