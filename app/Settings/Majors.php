<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Majors extends Model
{
    // Table name
    protected $table = 'spp_majoring_class';
    protected $primaryKey = 'majors_id';
    protected $fillable = [
        'majors_name',
        'majors_description',
        'majors_head',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];
}
