<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class PaymentParams extends Model
{
    // Table name
    protected $table = 'spp_payment_parameters';
    protected $primaryKey = 'payment_id';
    protected $fillable = [
        'payment_category_id',
        'ep_id',
        'payment_amount',
        'currency_id',
        'updated_by',
        'updated_by'
    ];

    public function category()
    {
        return $this->belongsTo('App\Settings\PaymentCategory', 'payment_category_id');
    }

    public function ep()
    {
        return $this->belongsTo('App\Settings\EducationParams', 'ep_id');
    }
}
