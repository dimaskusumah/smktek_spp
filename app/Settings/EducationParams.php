<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class EducationParams extends Model
{
    // Table name
    protected $table = 'spp_education_parameters';
    protected $primaryKey = 'ep_id';
    protected $fillable = [
        'ep_year',
        'ep_curriculum_year',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ];
}
