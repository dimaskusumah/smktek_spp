<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class PaymentCategory extends Model
{
    // Table name
    protected $table = 'spp_payment_category';
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'category_code',
        'category_name',
        'category_description',
        'category_status',
        'updated_by',
        'updated_by'
    ];

}
