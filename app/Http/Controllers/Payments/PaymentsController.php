<?php

namespace App\Http\Controllers\Payments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Payments\Invoices;
use App\Payments\Paid;
use App\Settings\PaymentCategory;
use App\Settings\Student;
use App\Settings\SchoolInfo;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = Invoices::with('category', 'student', 'student.majors')->get();

            return $getData;

        }

        // Return method
        return view('payments.invoices.index', compact('user', 'uriPath', 'menu'));
    }

    public function store(Request $request)
    {

        $categCode = PaymentCategory::find($request->category_id);

        $postData = new Invoices;

        $postData->invoice_no = $categCode->category_code .'.'. time();
        $postData->category_id = $request->category_id;
        $postData->student_id = $request->student_id;
        $postData->invoice_date = date('Y-m-d');
        $postData->invoice_amount = $request->invoice_amount;
        $postData->invoice_due_date = $request->invoice_due_date;
        $postData->invoice_status = 0;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataInv = $postData->save();

        if($postDataInv)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Invoice',
                    'message' => 'Invoice (yang belum dibayar) berhasil dibuat.',
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Invoice',
                    'message' => 'Invoice (yang belum dibayar) gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function update(Request $request, $id)
    {

        $putData = Invoices::find($id);

        $putData->invoice_status = 1;

        $putDataInv = $putData->save();

        if($putDataInv)
        {

            $postData = new Paid;

            $postData->invoice_id = $request->invoice_id;
            $postData->paid_amount = $request->paid_amount;
            $postData->paid_notes = $request->paid_notes;
            $postData->paid_date = date('Y-m-d');
            $postData->created_by = Auth::user()->id;
            $postData->created_at = date('Y-m-d');

            $postData->save();

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Invoice',
                    'message' => 'Invoice '. $request->invoice_no .' sudah dibayar. Silahkan cetak bukti pembayaran.',
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Invoice',
                    'message' => 'Invoice '. $request->invoice_no .' gagal disimpan!'
                ]
            ];

        }

        return response()->json($message);

    }

    public function show(Request $request, $id)
    {

        $params = $request->action;

        if($params == 'print')
        {

            $dataInvoice = Invoices::with('paid', 'category', 'student', 'student.majors')->where('spp_student_payment_invoice.invoice_no', $id)->get();

            $dataSchool = SchoolInfo::find(1);

            // Return method
            return view('payments.invoices.print', compact('dataInvoice', 'dataSchool'));
            // return $dataInvoice;

        }

    }

}
