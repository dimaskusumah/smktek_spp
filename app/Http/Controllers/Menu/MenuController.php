<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Role;
use Auth;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function navigation($roleId)
    {
        $uid = Auth::user()->id;

        $menu = Role::with('menus', 'menus.child')
                ->where([
                    ['roles_id', $roleId]
                ])->get();

        return $menu;
    }
}
