<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Payments\Invoices;
use App\Payments\Paid;
use App\Settings\PaymentCategory;
use App\Settings\Student;
use App\Settings\Majors;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = Invoices::with('category', 'student', 'student.majors', 'paid')
                               ->wherehas('paid', function($q){
                                   $q->whereRaw('YEARWEEK(paid_date, 1)');
                               })
                               ->where('invoice_status', 1)
                               ->get();

            return $getData;

        }

        // Return method
        return view('reports.reports.index', compact('user', 'uriPath', 'menu'));
    }
}
