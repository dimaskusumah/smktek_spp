<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class MasterController extends Controller
{
    // Check Login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dataRequests(Request $request, $module)
    {

        if($module == 'majors')
        {
            if(isset($request->search))
            {
                $majors = DB::table('spp_majoring_class')
                        ->select('majors_id as id', 'majors_name as text')
                        ->whereRaw('majors_name like "%'. $request->search .'%"')
                        ->orderBy('majors_name', 'asc')
                        ->get();
            } else {
                $majors = DB::table('spp_majoring_class')
                        ->select('majors_id as id', 'majors_name as text')
                        ->orderBy('majors_name', 'asc')
                        ->get();
            }

            return ['results' => $majors];
        }

        if($module == 'payment-category')
        {
            if(isset($request->search))
            {
                $category = DB::table('spp_payment_category as pc')
                        ->leftJoin('spp_payment_parameters as pp', 'pp.payment_category_id', 'pc.category_id')
                        ->select('category_id as id', 'category_name as text', 'payment_amount as amount')
                        ->whereRaw('category_name like "%'. $request->search .'%"')
                        ->orderBy('category_name', 'asc')
                        ->get();
            } else {
                $category = DB::table('spp_payment_category as pc')
                        ->leftJoin('spp_payment_parameters as pp', 'pp.payment_category_id', 'pc.category_id')
                        ->select('category_id as id', 'category_name as text', 'payment_amount as amount')
                        ->orderBy('category_name', 'asc')
                        ->get();
            }

            return ['results' => $category];
        }

        if($module == 'ep')
        {
            if(isset($request->search))
            {
                $ep = DB::table('spp_education_parameters')
                        ->select('ep_id as id', 'ep_year as text')
                        ->whereRaw('ep_year like "%'. $request->search .'%"')
                        ->orderBy('ep_year', 'asc')
                        ->get();
            } else {
                $ep = DB::table('spp_education_parameters')
                        ->select('ep_id as id', 'ep_year as text')
                        ->orderBy('ep_year', 'asc')
                        ->get();
            }

            return ['results' => $ep];
        }

        if($module == 'students')
        {
            if(isset($request->search))
            {
                $student = DB::table('spp_student')
                        ->select('student_id as id', 'student_name as text')
                        ->whereRaw('student_name like "%'. $request->search .'%"')
                        ->orderBy('student_name', 'asc')
                        ->get();
            } else {
                $student = DB::table('spp_student')
                        ->select('student_id as id', 'student_name as text')
                        ->orderBy('student_name', 'asc')
                        ->get();
            }

            return ['results' => $student];
        }

    }
}
