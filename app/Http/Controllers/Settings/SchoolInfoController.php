<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\SchoolInfo;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class SchoolInfoController extends Controller
{
    // Check Login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        $getData = SchoolInfo::find(1);

        // Return method
        return view('settings.school.index', compact('user', 'uriPath', 'menu', 'getData'));

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

        $putData = SchoolInfo::find($id);

        $putData->school_name = $request->school_name;
        $putData->school_headmaster = $request->school_headmaster;
        $putData->school_address = $request->school_address;
        $putData->school_phone = $request->school_phone;
        $putData->school_fax = $request->school_fax;
        $putData->school_description = $request->school_description;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataSchool = $putData->save();

        if($putDataSchool)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Informasi Sekolah',
                    'message' => 'Informasi Sekolah berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Informasi Sekolah',
                    'message' => 'Informasi Sekolah gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }

}
