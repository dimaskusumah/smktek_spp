<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\PaymentCategory;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class PayCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = PaymentCategory::all();

            return $getData;

        }

        // Return method
        return view('settings.payment-category.index', compact('user', 'uriPath', 'menu'));
    }

    public function store(Request $request)
    {

        $postData = new PaymentCategory;

        $postData->category_code = $request->category_code;
        $postData->category_name = $request->category_name;
        $postData->category_description = $request->category_description;
        $postData->category_status = $request->category_status;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataPayment = $postData->save();

        if($postDataPayment)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Kategori Pembayaran',
                    'message' => 'Kategori Pembayaran berhasil dibuat.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Kategori Pembayaran',
                    'message' => 'Kategori Pembayaran gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

        $putData = PaymentCategory::find($id);

        $putData->category_code = $request->category_code;
        $putData->category_name = $request->category_name;
        $putData->category_description = $request->category_description;
        $putData->category_status = $request->category_status;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataPayment = $putData->save();

        if($putDataPayment)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Kategori Pembayaran',
                    'message' => 'Kategori Pembayaran berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Kategori Pembayaran',
                    'message' => 'Kategori Pembayaran gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }

}
