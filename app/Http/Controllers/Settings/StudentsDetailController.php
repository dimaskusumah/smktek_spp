<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\Students;
use App\Settings\StudentsDetail;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class StudentsDetailController extends Controller
{
    // Check login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request, $id)
    {

        $putData = StudentsDetail::find($id);

        $putData->student_birth_place = $request->student_birth_place;
        $putData->student_birth_day = $request->student_birth_day;
        $putData->student_address = $request->student_address;
        $putData->student_phone = $request->student_phone;
        $putData->student_father_name = $request->student_father_name;
        $putData->student_father_jobdesc_id = $request->student_father_jobdesc_id;
        $putData->student_father_phone = $request->student_father_phone;
        $putData->student_mother_name = $request->student_mother_name;
        $putData->student_mother_jobdesc_id = $request->student_mother_jobdesc_id;
        $putData->student_mother_phone = $request->student_mother_phone;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataStudent = $putData->save();

        if($putDataStudent)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Detail Data Siswa',
                    'message' => 'Detail Data Siswa berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Detail Data Siswa',
                    'message' => 'Detail Data Siswa gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }
}
