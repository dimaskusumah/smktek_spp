<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\EducationParams;

class EducationParamsController extends Controller
{
    // Check Login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
            $getData = EducationParams::all();

            return $getData;
    }

    public function store(Request $request)
    {

        $postData = new EducationParams;

        $postData->ep_year = $request->ep_year;
        $postData->ep_curriculum_year = $request->ep_curriculum_year;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataEp = $postData->save();

        if($postDataEp)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Tahun Ajaran',
                    'message' => 'Tahun Ajaran berhasil dibuat.',
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Tahun Ajaran',
                    'message' => 'Tahun Ajaran gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function update(Request $request, $id)
    {

        $postData = EducationParams::find($id);

        $postData->ep_year = $request->ep_year;
        $postData->ep_curriculum_year = $request->ep_curriculum_year;
        $postData->updated_by = Auth::user()->id;
        $postData->updated_at = date('Y-m-d');

        $postDataEp = $postData->save();

        if($postDataEp)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Tahun Ajaran',
                    'message' => 'Tahun Ajaran berhasil di ubah.',
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Tahun Ajaran',
                    'message' => 'Tahun Ajaran gagal di ubah.'
                ]
            ];

        }

        return response()->json($message);

    }

}
