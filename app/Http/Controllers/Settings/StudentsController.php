<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\Students;
use App\Settings\StudentsDetail;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class StudentsController extends Controller
{
    // Check Login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = Students::with('majors', 'details')->get();

            return $getData;

        }

        // Return method
        return view('settings.students.index', compact('user', 'uriPath', 'menu'));
    }

    public function store(Request $request)
    {

        $postData = new Students;

        $postData->student_register_number = $request->student_register_number;
        $postData->student_nisn = $request->student_nisn;
        $postData->student_name = $request->student_name;
        $postData->student_gender = $request->student_gender;
        $postData->student_school_year = $request->student_school_year;
        $postData->student_class = $request->student_class;
        $postData->student_school_majors = $request->student_school_majors;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataStudent = $postData->save();

        if($postDataStudent)
        {

            $postDataDetail = new StudentsDetail;

            $postDataDetail->student_id = $postData->student_id;
            $postDataDetail->student_address = ' ';
            $postDataDetail->student_phone = '+62';
            $postDataDetail->created_by = Auth::user()->id;
            $postDataDetail->created_at = date('Y-m-d');

            $postDetail = $postDataDetail->save();

            if($postDetail)
            {
                $message = [
                    [
                        'status' => 'success',
                        'type' => 'Data Siswa',
                        'message' => 'Data Siswa berhasil dibuat.',
                    ]
                ];
            }

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Data Siswa',
                    'message' => 'Data Siswa gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

        $putData = Students::find($id);

        $putData->student_register_number = $request->student_register_number;
        $putData->student_nisn = $request->student_nisn;
        $putData->student_name = $request->student_name;
        $putData->student_gender = $request->student_gender;
        $putData->student_school_year = $request->student_school_year;
        $putData->student_class = $request->student_class;
        $putData->student_school_majors = $request->student_school_majors;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataStudent = $putData->save();

        if($putDataStudent)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Data Siswa',
                    'message' => 'Data Siswa berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Data Siswa',
                    'message' => 'Data Siswa gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }
}
