<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\Majors;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class MajorsController extends Controller
{
    // Check Login status
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = Majors::all();

            return $getData;

        }

        // Return method
        return view('settings.majoring-class.index', compact('user', 'uriPath', 'menu'));
    }

    public function store(Request $request)
    {

        $postData = new Majors;

        $postData->majors_name = $request->majors_name;
        $postData->majors_description = $request->majors_description;
        $postData->majors_head = $request->majors_head;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataMajors = $postData->save();

        if($postDataMajors)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Data Jurusan',
                    'message' => 'Data Jurusan berhasil dibuat.',
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Data Jurusan',
                    'message' => 'Data Jurusan gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

        $putData = Majors::find($id);

        $putData->majors_name = $request->majors_name;
        $putData->majors_description = $request->majors_description;
        $putData->majors_head = $request->majors_head;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataMajors = $putData->save();

        if($putDataMajors)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Data Jurusan',
                    'message' => 'Data Jurusan berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Data Jurusan',
                    'message' => 'Data Jurusan gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }

}
