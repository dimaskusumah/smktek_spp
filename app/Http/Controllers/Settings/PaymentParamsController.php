<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Role;
use App\Settings\PaymentParams;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class PaymentParamsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        if($request->params == 'data')
        {

            $getData = PaymentParams::with('category', 'ep')->get();

            return $getData;

        }

        // Return method
        return view('settings.payment-parameter.index', compact('user', 'uriPath', 'menu'));
    }

    public function store(Request $request)
    {

        $postData = new PaymentParams;

        $postData->payment_category_id = $request->payment_category_id;
        $postData->ep_id = $request->ep_id;
        $postData->payment_amount = $request->payment_amount;
        $postData->created_by = Auth::user()->id;
        $postData->created_at = date('Y-m-d');

        $postDataPayment = $postData->save();

        if($postDataPayment)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Parameter Pembayaran',
                    'message' => 'Parameter Pembayaran berhasil dibuat.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Parameter Pembayaran',
                    'message' => 'Parameter Pembayaran gagal dibuat.'
                ]
            ];

        }

        return response()->json($message);

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

        $putData = PaymentParams::find($id);

        $putData->payment_category_id = $request->payment_category_id;
        $putData->payment_amount = $request->payment_amount;
        $putData->ep_id = $request->ep_id;
        $putData->updated_by = Auth::user()->id;
        $putData->updated_at = date('Y-m-d');

        $putDataPayment = $putData->save();

        if($putDataPayment)
        {

            $message = [
                [
                    'status' => 'success',
                    'type' => 'Ubah Parameter Pembayaran',
                    'message' => 'Parameter Pembayaran berhasil diubah.'
                ]
            ];

        } else {

            $message = [
                [
                    'status' => 'error',
                    'type' => 'Ubah Parameter Pembayaran',
                    'message' => 'Parameter Pembayaran gagal diubah.'
                ]
            ];

        }

        return response()->json($message);

    }
}
