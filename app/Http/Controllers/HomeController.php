<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Settings\Students;
use App\Payments\Invoices;
use App\Http\Controllers\Menu\MenuController;
use App\Http\Controllers\Header\HeaderController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // Get uid
        $uid = Auth::user()->id;
        // Fetch user and role detail
        $user = User::with('roles')->where('id', $uid)->get();
        $roleId = $user[0]->roles[0]->roles_id;

        // Call new MenuController
        $navigate = new MenuController();
        // Extract menu items
        $navi = $navigate->navigation($roleId);
        $menu = $navi[0]->menus;
        // Use in title section
        $uriPath = array_slice(explode('/', url()->current()), 3);

        $dataSiswa = Students::sum('student_id');

        $unpaidInvoice = Invoices::where('invoice_status', 0)->get();
        $sumUnpaid = $unpaidInvoice->count();

        $paidInvoice = Invoices::where('invoice_status', 1)->get();
        $sumPaid = $paidInvoice->count();

        $chartData = DB::table('spp_student_payment_invoice as inv')
                       ->leftJoin('spp_student_payment_invoice_paid as paid', 'paid.invoice_id', 'inv.invoice_id')
                       ->selectRaw('
                            DATE_FORMAT(invoice_date, "%b-%Y") as date,
                            IF(
                                invoice_amount IS NULL,
                                0,
                                SUM(invoice_amount)
                            ) as invoice,
                            IF(
                                paid_amount IS NULL,
                                0,
                                SUM(paid_amount)
                            ) as paid
                          ')
                        ->groupBy(DB::raw('MONTH(invoice_date)'))
                        ->get();

        // Return method
        return view('dashboard.dashboard', compact('user', 'uriPath', 'menu', 'dataSiswa', 'sumPaid', 'sumUnpaid', 'chartData'));
    }

    // User session check
    public function userSession()
    {
        if(Auth::user()->id)
        {
            return redirect('/dashboard');
        }
    }
}
