<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Table name
    protected $table = 'spp_user_roles';

    protected $primaryKey = 'roles_id';

    public function menus()
    {
        return $this->belongsToMany(
                Menu::class,
                'spp_roles_match_menu',
                'roles_id',
                'menu_id')
            ->where('menu_parent_id', '=', null)
            ->orderBy('menu_parent_id', 'ASC');
    }
}
