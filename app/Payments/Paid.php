<?php

namespace App\Payments;

use Illuminate\Database\Eloquent\Model;

class Paid extends Model
{
    // Table name
    protected $table = 'spp_student_payment_invoice_paid';
    protected $primaryKey = 'paid_id';
    public $timestamps = false;
    protected $fillable = [
        'invoice_id',
        'paid_amount',
        'paid_date',
        'paid_notes',
        'created_by',
        'created_at'
    ];
}
