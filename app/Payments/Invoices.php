<?php

namespace App\Payments;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    // Table name
    protected $table = 'spp_student_payment_invoice';
    protected $primaryKey = 'invoice_id';
    public $timestamps = false;
    protected $fillable = [
        'invoice_no',
        'category_id',
        'student_id',
        'invoice_amount',
        'invoice_date',
        'invoice_due_date',
        'invoice_notes',
        'created_by',
        'created_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Settings\PaymentCategory', 'category_id');
    }

    public function student()
    {
        return $this->belongsTo('App\Settings\Students', 'student_id');
    }

    public function paid()
    {
        return $this->hasOne('App\Payments\Paid', 'invoice_id');
    }
}
