<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    // Table name
    protected $table = 'spp_apps_menu';
}
