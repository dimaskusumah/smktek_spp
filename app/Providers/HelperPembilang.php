<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperPembilang extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Pembilang
        require_once app_path('Helpers/Pembilang.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
