<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    // Table name
    protected $table = 'spp_apps_menu';

    protected $primaryKey = 'menu_id';

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'spp_roles_match_menu', 'menu_id', 'roles_id');
    }

    public function child()
    {
        return $this->hasMany(SubMenu::class, 'menu_parent_id')->where('menu_status', 1);
    }
}
