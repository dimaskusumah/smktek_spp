$(document).ready(function(){

    var buttons = [
        {
            extend: 'pdfHtml5',
            pageSize: 'A4',
            title: '',
            footer: true,
            messageTop: 'Laporan Pembayaran Mingguan'
        },
    ];

    var reportsTable = $('#reportsData').DataTable({
        dom: 'Bfrtip',
        buttons: buttons,
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/reports?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'student.student_name' },
            {
                data: 'student.student_class',
                render: function(data, type, row){
                    return row.student.student_class + ' ' + row.student.majors.majors_name
                }
            },
            { data: 'category.category_name' },
            {
                data: 'paid.paid_amount',
                className: 'text-right',
                render: $.fn.DataTable.render.number(',', '.', 0, '')
            },
            {
                data: 'paid.paid_date',
                render: $.fn.DataTable.render.moment('DD MMM YYYY')
            },
        ],
        footerCallback: function ( row, data, start, end, display ){
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            api.columns('.sum').every( function () {
                var numFormat = $.fn.dataTable.render.number( ',', '.', 0 ).display;
                var sum = this
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                this.footer().innerHTML = numFormat(sum);
            } );

        },
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Laporan Pembayaran" }
    });

});
