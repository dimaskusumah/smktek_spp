$(document).ready(function(){

    var epTable = $('#epData').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/settings/ep',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'ep_year' },
            { data: 'ep_curriculum_year' },
            {
                data: 'ep_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    var edit = '<button class="edit-ep btn btn-xs btn-info" style="color: #FFF;" title="Ubah"><i class="fa fa-fw fa-pencil-square-o"></i></button>';

                    return edit;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Tahun Ajaran" }
    });

    $('#save').on('click', function(e){
        if( !$('#form-ep').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                ep_year: $('[name=ep_year]').val(),
                ep_curriculum_year: $('[name=ep_curriculum_year]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/settings/ep',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    epTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-ep").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#epData tbody').on( 'click', 'button.edit-ep', function () {

        $("#modal-default").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();

        var tr = $(this).closest('tr');
        var rownya = epTable.row( tr );
        var datanya = rownya.data();

        $('[name=ep_id]').val(datanya.ep_id);
        $('[name=ep_year]').val(datanya.ep_year);
        $('[name=ep_curriculum_year]').val(datanya.ep_curriculum_year);

    });

    $('#edit').on('click', function(e){
        if( !$('#form-ep').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                ep_year: $('[name=ep_year]').val(),
                ep_curriculum_year: $('[name=ep_curriculum_year]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/ep/'+ $('[name=ep_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    epTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-ep").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-ep").reset();
    })

});
