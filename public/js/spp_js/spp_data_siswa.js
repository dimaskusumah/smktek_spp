$(document).ready(function(){

    var studentsTable = $('#studentsData').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $('#info-details').show();
                    $('#form-details').hide();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/settings/students?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'student_register_number' },
            { data: 'student_nisn' },
            { data: 'student_name' },
            { data: 'student_gender' },
            {
                data: 'student_class',
                render: function(data, type, row){
                    return row.student_class + ' ' + row.majors.majors_name
                }
            },
            {
                data: 'student_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    var edit = '<button class="edit-student btn btn-xs btn-info" style="color: #FFF;" title="Ubah"><i class="fa fa-fw fa-pencil-square-o"></i></button>';

                    return edit;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Siswa" }
    });

    $('#genders').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        data: [
            {id: 'L', text: 'Laki - Laki'},
            {id: 'P', text: 'Perempuan'}
        ]
    });

    $('#classes').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        data: [
            {id: 'X', text: 'X'},
            {id: 'XI', text: 'XI'},
            {id: 'XXI', text: 'XII'},
        ]
    });

    $('#status').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        data: [
            {id: '1', text: 'Aktif'},
            {id: '2', text: 'Lulus'},
            {id: '3', text: 'Drop Out'},
        ]
    });

    var majorSelect = $('#majors').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        ajax: {
            url: '/master/data/majors',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                console.log(params);
                // Query parameters will be ?search=[term]&type=public
                return query;
            }
        }
    });

    $('#save').on('click', function(e){
        if( !$('#form-students').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                student_register_number: $('[name=student_register_number]').val(),
                student_nisn: $('[name=student_nisn]').val(),
                student_name: $('[name=student_name]').val(),
                student_gender: $('[name=student_gender]').val(),
                student_school_year: $('[name=student_school_year]').val(),
                student_class: $('[name=student_class]').val(),
                student_school_majors: $('[name=student_school_majors]').val(),
                student_status: $('[name=student_status]').val(),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/settings/students',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    studentsTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-students").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#studentsData tbody').on( 'click', 'button.edit-student', function () {

        $("#modal-default").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();
        $('#info-details').hide();
        $('#form-details').show();

        document.getElementById('student_nisn').disabled = true;
        document.getElementById('student_register_number').disabled = true;

        var tr = $(this).closest('tr');
        var rownya = studentsTable.row( tr );
        var datanya = rownya.data();

        var statusnya = '';

        if(datanya.student_gender == 'L'){
            gendernya = 'Laki - Laki';
        } else {
            gendernya = 'Perempuan';
        }

        if(datanya.student_class == 'X'){
            kelasnya = 'X';
        } else if(datanya.student_class = 'XI') {
            kelasnya = 'XI';
        } else {
            kelasnya = 'XII';
        }

        if(datanya.student_status == 1){
            statusnya = 'Aktif';
        } else if(datanya.student_status = 2) {
            statusnya = 'Lulus';
        } else {
            statusnya = 'Drop Out';
        }

        var dateT = $("#student_birth_day").flatpickr({
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "Y-m-d",
            defaultDate: datanya.details.student_birth_day
        });

        $('[name=student_id]').val(datanya.student_id);
        $('[name=student_register_number]').val(datanya.student_register_number);
        $('[name=student_nisn]').val(datanya.student_nisn);
        $('[name=student_name]').val(datanya.student_name);
        $('#genders').append('<option value="'+ datanya.student_gender +'" selected="selected">'+ gendernya +'</option>');
        $('[name=student_school_year]').val(datanya.student_school_year),
        $('#classes').append('<option value="'+ datanya.student_class +'" selected="selected">'+ kelasnya +'</option>');
        $('#majors').append('<option value="'+ datanya.student_school_majors +'" selected="selected">'+ datanya.majors.majors_name +'</option>');
        $('#status').append('<option value="'+ datanya.student_status +'" selected="selected">'+ statusnya +'</option>');

        // Field Details
        $('[name=student_detail_id]').val(datanya.details.student_detail_id);
        $('[name=student_birth_place]').val(datanya.details.student_birth_place);
        $('[name=student_birth_day]').val(datanya.details.student_birth_day);
        $('[name=student_address]').val(datanya.details.student_address);
        $('[name=student_phone]').val(datanya.details.student_phone);
        $('[name=student_father_name]').val(datanya.details.student_father_name);
        $('[name=student_father_jobdesc_id]').val(datanya.details.student_father_jobdesc_id);
        $('[name=student_father_phone]').val(datanya.details.student_father_phone);
        $('[name=student_mother_name]').val(datanya.details.student_mother_name);
        $('[name=student_mother_jobdesc_id]').val(datanya.details.student_mother_jobdesc_id);
        $('[name=student_mother_phone]').val(datanya.details.student_mother_phone);

    });

    $('#edit').on('click', function(e){
        if( !$('#form-students').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                student_register_number: $('[name=student_register_number]').val(),
                student_nisn: $('[name=student_nisn]').val(),
                student_name: $('[name=student_name]').val(),
                student_gender: $('[name=student_gender]').val(),
                student_school_year: $('[name=student_school_year]').val(),
                student_class: $('[name=student_class]').val(),
                student_school_majors: $('[name=student_school_majors]').val(),
                student_status: $('[name=student_status]').val(),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/students/'+ $('[name=student_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    studentsTable.ajax.reload();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#edit-detail').on('click', function(e){
        if( !$('#form-details').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                student_birth_place: $('[name=student_birth_place]').val(),
                student_birth_day: $('[name=student_birth_day]').val(),
                student_address: $('[name=student_address]').val(),
                student_phone: $('[name=student_phone]').val(),
                student_father_name: $('[name=student_father_name]').val(),
                student_father_jobdesc_id: $('[name=student_father_jobdesc_id]').val(),
                student_father_phone: $('[name=student_father_phone]').val(),
                student_mother_name: $('[name=student_mother_name]').val(),
                student_mother_jobdesc_id: $('[name=student_mother_jobdesc_id]').val(),
                student_mother_phone: $('[name=student_mother_phone]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/students-detail/'+ $('[name=student_detail_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    studentsTable.ajax.reload();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-students").reset();
        document.getElementById("form-details").reset();
        document.getElementById('student_nisn').disabled = false;
        document.getElementById('student_register_number').disabled = false;
        $('#genders').val('').trigger('change');
        $('#classes').val('').trigger('change');
        $('#majors').val('').trigger('change');
        $('#status').val('').trigger('change');
    });

});
