$(document).ready(function(){

    function removeComa(number){
        var clear = number.replace(/\,/g, "");
        return clear;
    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    var dateT = $("#invoice_due_date").flatpickr({
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
    });

    var cleave = new Cleave('.money', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    var cleave = new Cleave('.money1', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    var invoicesTable = $('#invoicesData').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    $('[name=category_code]').val('Otomatis Dibuat');
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/payments/invoices?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'invoice_no' },
            {
                data: 'student_id',
                render:  function(data, type, row){
                    return row.student.student_name;
                }
            },
            {
                data: 'student_id',
                render:  function(data, type, row){
                    return row.student.student_class+ ' ' +row.student.majors.majors_name;
                }
            },
            {
                data: 'category_id',
                render: function(data, type, row){
                    return row.category.category_name;
                }
            },
            {
                data: 'invoice_amount',
                className: 'text-right',
                render: $.fn.DataTable.render.number(',', '.', 0, ''),
                defaultContent: '0'
            },
            {
                data: 'invoice_due_date',
                render: $.fn.DataTable.render.moment('DD MMM YYYY'),
                defaultContent: 'Unpaid'
            },
            {
                data: 'payment_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    if(row.invoice_status == 1){
                        var btnPay = 'disabled';
                        var btnPrnt = '';
                        var status = 'Sudah dibayar';
                    } else {
                        var btnPay = '';
                        var btnPrnt = 'disabled';
                        var status = 'Bayar';
                    }

                    var paid = '<button class="pay-invoice btn btn-xs btn-success" style="color: #FFF;" title="'+ status +'" '+ btnPay +'><i class="fa fa-fw fa-check-square-o"></i></button>';
                    var print = '<a href="/payments/invoices/'+ row.invoice_no +'?action=print" class="print-invoice btn btn-xs btn-info" style="color: #FFF;" title="Cetak Invoice" '+ btnPrnt +' target="_blank"><i class="fa fa-fw fa-print"></i></a>';

                    return paid +' | '+ print;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Kategori Pembayaran" },
        rowCallback: function(row, data){
            if(data.invoice_status == 1){
                $('td:eq(0)', row).addClass( 'bg-success' );
                $('td:eq(1)', row).addClass( 'bg-success' );
                $('td:eq(2)', row).addClass( 'bg-success' );
                $('td:eq(3)', row).addClass( 'bg-success' );
                $('td:eq(4)', row).addClass( 'bg-success' );
                $('td:eq(5)', row).addClass( 'bg-success' );
            }
        }
    });

    var categorySelect = $('#category').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        ajax: {
            url: '/master/data/payment-category',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                console.log(params);
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
        }
    });

    $('#category').on('select2:select', function(e){
        var data = e.params.data;
        $('[name=invoice_amount]').val(formatNumber(data.amount));
    });

    var studentSelect = $('#student').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        ajax: {
            url: '/master/data/students',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                console.log(params);
                // Query parameters will be ?search=[term]&type=public
                return query;
            }
        }
    });

    $('#save').on('click', function(e){
        if( !$('#form-invoices').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                category_id: $('[name=category_id]').val(),
                student_id: $('[name=student_id]').val(),
                invoice_amount: removeComa($('[name=invoice_amount]').val()),
                invoice_due_date: $('[name=invoice_due_date]').val(),
                invoice_notes: $('[name=invoice_notes]').val(),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/payments/invoices',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    invoicesTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-invoices").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#invoicesData tbody').on( 'click', 'button.pay-invoice', function () {

        $("#modal-pay").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();

        var tr = $(this).closest('tr');
        var rownya = invoicesTable.row( tr );
        var datanya = rownya.data();

        $('[name=paid_invoice_id]').val(datanya.invoice_id);
        $('[name=paid_invoice_no]').val(datanya.invoice_no);
        $('[name=student]').val(datanya.student.student_name +' Kelas '+ datanya.student.student_class +' '+ datanya.student.majors.majors_name);
        $('[name=paid_amount]').val(formatNumber(datanya.invoice_amount));
        $('[name=paid_notes]').val(datanya.invoice_notes);

    });

    $('#pay').on('click', function(e){
        if( !$('#form-pay').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                invoice_id: $('[name=paid_invoice_id]').val(),
                paid_amount: removeComa($('[name=paid_amount]').val()),
                paid_notes: $('[name=paid_notes]').val(),
                invoice_no: $('[name=paid_invoice_no]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/payments/invoices/'+ $('[name=paid_invoice_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    invoicesTable.ajax.reload();
                    $('#modal-pay').modal('toggle');
                    document.getElementById("form-pay").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-invoices").reset();
        $('#category').val('').trigger('change');
        $('#student').val('').trigger('change');
    })

    $('#modal-pay').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-pay").reset();
    })

});
