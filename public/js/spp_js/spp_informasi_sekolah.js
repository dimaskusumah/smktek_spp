$(document).ready(function(){

    $('#save').on('click', function(e){
        if( !$('#form-school').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                school_name: $('[name=school_name]').val(),
                school_headmaster: $('[name=school_headmaster]').val(),
                school_address: $('[name=school_address]').val(),
                school_phone: $('[name=school_phone]').val(),
                school_fax: $('[name=school_fax]').val(),
                school_description: $('[name=school_description]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/school-parameter/'+ $('[name=school_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

});
