$(document).ready(function(){

    var majorsTable = $('#majoringData').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/settings/majoring-class?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'majors_name' },
            { data: 'majors_description' },
            { data: 'majors_head' },
            {
                data: 'majors_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    var edit = '<button class="edit-majors btn btn-xs btn-info" style="color: #FFF;" title="Ubah"><i class="fa fa-fw fa-pencil-square-o"></i></button>';

                    return edit;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Jurusan" }
    });

    $('#save').on('click', function(e){
        if( !$('#form-majors').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                majors_name: $('[name=majors_name]').val(),
                majors_description: $('[name=majors_description]').val(),
                majors_head: $('[name=majors_head]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/settings/majoring-class',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    majorsTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-majors").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#majoringData tbody').on( 'click', 'button.edit-majors', function () {

        $("#modal-default").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();

        var tr = $(this).closest('tr');
        var rownya = majorsTable.row( tr );
        var datanya = rownya.data();

        $('[name=majors_id]').val(datanya.majors_id);
        $('[name=majors_name]').val(datanya.majors_name);
        $('[name=majors_description]').val(datanya.majors_description);
        $('[name=majors_head]').val(datanya.majors_head);

    });

    $('#edit').on('click', function(e){
        if( !$('#form-majors').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                majors_name: $('[name=majors_name]').val(),
                majors_description: $('[name=majors_description]').val(),
                majors_head: $('[name=majors_head]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/majoring-class/'+ $('[name=majors_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    majorsTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-majors").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-majors").reset();
    })

});
