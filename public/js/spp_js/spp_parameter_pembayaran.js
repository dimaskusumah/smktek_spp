$(document).ready(function(){

    function removeComa(number){
        var clear = number.replace(/\,/g, "");
        return clear;
    }

    var cleave = new Cleave('.money', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    var paymentTable = $('#payParams').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/settings/payment-parameter?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            {
                data: 'payment_category_id',
                render: function(data, type, row){
                    return row.category.category_name;
                }
            },
            {
                data: 'ep_id',
                render:  function(data, type, row){
                    return row.ep.ep_year;
                }
            },
            {
                data: 'payment_amount',
                className: 'text-right',
                render: $.fn.DataTable.render.number(',', '.', 0, ''),
            },
            {
                data: 'payment_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    var edit = '<button class="edit-payment btn btn-xs btn-info" style="color: #FFF;" title="Ubah"><i class="fa fa-fw fa-pencil-square-o"></i></button>';

                    return edit;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Parameter Pembayaran" }
    });

    var categorySelect = $('#category').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        ajax: {
            url: '/master/data/payment-category',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                console.log(params);
                // Query parameters will be ?search=[term]&type=public
                return query;
            }
        }
    });

    var epSelect = $('#ep').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        ajax: {
            url: '/master/data/ep',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                console.log(params);
                // Query parameters will be ?search=[term]&type=public
                return query;
            }
        }
    });

    $('#save').on('click', function(e){
        if( !$('#form-payment-parameter').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                payment_category_id: $('[name=payment_category_id]').val(),
                ep_id: $('[name=ep_id]').val(),
                payment_amount: removeComa($('[name=payment_amount]').val()),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/settings/payment-parameter',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    paymentTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-payment-parameter").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#payParams tbody').on( 'click', 'button.edit-payment', function () {

        $("#modal-default").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();

        var tr = $(this).closest('tr');
        var rownya = paymentTable.row( tr );
        var datanya = rownya.data();

        $('[name=payment_id]').val(datanya.payment_id);
        $('[name=payment_amount]').val(datanya.payment_amount);
        $('#category').append('<option value="'+ datanya.payment_category_id +'" selected="selected">'+ datanya.category.category_name +'</option>');
        $('#ep').append('<option value="'+ datanya.ep_id +'" selected="selected">'+ datanya.ep.ep_year +'</option>');

    });

    $('#edit').on('click', function(e){
        if( !$('#form-payment-parameter').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                payment_category_id: $('[name=payment_category_id]').val(),
                ep_id: $('[name=ep_id]').val(),
                payment_amount: removeComa($('[name=payment_amount]').val()),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/payment-parameter/'+ $('[name=payment_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    paymentTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-payment-parameter").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Cool'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-payment-parameter").reset();
        $('#category').val('').trigger('change');
        $('#ep').val('').trigger('change');
    })

});
