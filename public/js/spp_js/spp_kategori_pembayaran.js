$(document).ready(function(){

    var paymentTable = $('#payCategory').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Baru',
                className: 'btn btn-primary text-white',
                action: function ( e, dt, node, config ) {
                    var rand = Math.floor((Math.random() * 999) + 1);
                    $('[name=category_code]').val('KP'+ rand);
                    document.getElementById('category_code').disabled = true;
                    $("#header-label").text("Tambah");
                    $('#edit').hide();
                    $('#save').show();
                    $("#modal-default").modal();
                }
            }
        ],
        processing: true,
        lengthMenu: [[5, 25, 50, -1], [5, 25, 50, "All"]],
        ajax: {
            url: '/settings/payment-category?params=data',
            dataSrc: ''
        },
        dataSrc: '',
        columns: [
            { data: 'category_code' },
            { data: 'category_name' },
            { data: 'category_description' },
            {
                data: 'category_status',
                className: 'text-center',
                render: function(data, type, row, meta){
                    if(data == 1){
                        return 'Aktif';
                    } else {
                        return 'Tidak Aktif';
                    }
                }
            },
            {
                data: 'category_id',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row, meta){
                    var edit = '<button class="edit-payment btn btn-xs btn-info" style="color: #FFF;" title="Ubah"><i class="fa fa-fw fa-pencil-square-o"></i></button>';

                    return edit;
                }
            }
        ],
        language: { info: "Menampilkan _END_ dari _TOTAL_ data Kategori Pembayaran" }
    });

    $('#status').select2({
        theme: 'bootstrap',
        placeholder: 'Please Select Option',
        data: [
            {id: 0, text: 'Tidak Aktif'},
            {id: 1, text: 'Aktif'}
        ]
    });

    $('#save').on('click', function(e){
        if( !$('#form-payment-category').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                category_code: $('[name=category_code]').val(),
                category_name: $('[name=category_name]').val(),
                category_description: $('[name=category_description]').val(),
                category_status: $('[name=category_status]').val(),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: data,
                url: '/settings/payment-category',
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    paymentTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-payment-category").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#payCategory tbody').on( 'click', 'button.edit-payment', function () {

        $("#modal-default").modal();
        $("#header-label").text("Ubah");
        $('#edit').show();
        $('#save').hide();
        document.getElementById('category_code').disabled = true;

        var tr = $(this).closest('tr');
        var rownya = paymentTable.row( tr );
        var datanya = rownya.data();

        var statusnya = '';

        if(datanya.category_status == 1){
            statusnya = 'Aktif';
        } else {
            statusnya = 'Tidak Aktif';
        }

        $('[name=category_id]').val(datanya.category_id);
        $('[name=category_code]').val(datanya.category_code);
        $('[name=category_name]').val(datanya.category_name);
        $('[name=category_description]').val(datanya.category_description);
        $('#status').append('<option value="'+ datanya.category_status +'" selected="selected">'+ statusnya +'</option>');

    });

    $('#edit').on('click', function(e){
        if( !$('#form-payment-category').parsley().validate() ){

            e.preventDefault();

        } else {

            var data = {
                category_code: $('[name=category_code]').val(),
                category_name: $('[name=category_name]').val(),
                category_description: $('[name=category_description]').val(),
                category_status: $('[name=category_status]').val(),
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                data: data,
                url: '/settings/payment-category/'+ $('[name=category_id]').val(),
                success: function(msg) {
                    Swal.fire({
                        title: 'Sukses',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                    paymentTable.ajax.reload();
                    $('#modal-default').modal('toggle');
                    document.getElementById("form-payment-category").reset();
                },
                error: function(msg) {
                    Swal.fire({
                        title: 'Gagal!',
                        text: msg[0].message,
                        type: msg[0].status,
                        confirmButtonText: 'Tutup'
                    });
                }
            });

        }

    });

    $('#modal-default').on('hide.bs.modal', function (e) {
        // do something...
        document.getElementById("form-payment-category").reset();
        document.getElementById('category_code').disabled = false;
        $('#status').val('').trigger('change');
    })

});
