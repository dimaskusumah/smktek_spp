<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', 'HomeController@userSession');
Route::resource('/dashboard', 'HomeController');
Route::resource('/settings/payment-category', 'Settings\PayCategoryController');
Route::resource('/settings/students', 'Settings\StudentsController');
Route::resource('/settings/students-detail', 'Settings\StudentsDetailController');
Route::resource('/settings/school-parameter', 'Settings\SchoolInfoController');
Route::resource('/settings/majoring-class', 'Settings\MajorsController');
Route::resource('/settings/apps', 'Settings\AppsController');
Route::resource('/settings/ep', 'Settings\EducationParamsController');
Route::resource('/settings/payment-parameter', 'Settings\PaymentParamsController');
Route::resource('/payments/invoices', 'Payments\PaymentsController');
Route::resource('/reports', 'Reports\ReportsController');

// Master Data
Route::get('/master/data/{module}', 'Master\MasterController@dataRequests');
